package com.javaleague.oo.exercise.pay.refactor;

public class ObjectWrapper {
	
	private final Object value;

	public ObjectWrapper(Object value) {
		super();
		this.value = value;
	}

	public Object getValue() {
		return value;
	}
	
}
