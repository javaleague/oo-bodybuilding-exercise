package com.javaleague.oo.exercise.pay.refactor;

import com.javaleague.oo.exercise.pay.refactor.Name.FirstName;
import com.javaleague.oo.exercise.pay.refactor.Name.LastName;

public class Paperboy {
    private Customer myCustomer;
    public void pay(float payment) {
    	myCustomer.pay(new Money(payment));
    }
    
    public void setMyCustomer(Customer myCustomer) {
		this.myCustomer = myCustomer;
	}
    
    public static void main(String[] args) {
    	Paperboy zhangsan = new Paperboy();
    	Name name = new Name(new FirstName("si"), new LastName("li"));
    	Wallet wallet = new Wallet(new Money(Float.valueOf("30")));
    	Customer lisi = new Customer(name, wallet);
    	//购买
    	float amount = 15;
    	zhangsan.setMyCustomer(lisi);
    	System.out.println("开始支付");
    	zhangsan.pay(amount);
    	System.out.println("支付成功");
	}
    
}
