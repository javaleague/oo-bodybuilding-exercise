package com.javaleague.oo.exercise.pay.refactor;

public class Wallet {
	
    public Wallet(Money money) {
		super();
		this.money = money;
	}
	private Money money;
    public float getTotalMoney() {
        return money.getValue();
    }
    public void putMoney(float deposit) {
    	money = money.add(deposit);
    }
    public void takeMoney(float debit) {
    	money = money.add(-debit);
    }
}
