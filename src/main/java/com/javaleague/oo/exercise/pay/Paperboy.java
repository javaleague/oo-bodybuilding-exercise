package com.javaleague.oo.exercise.pay;

public class Paperboy {
    private Customer myCustomer;
    public void pay(float payment) {
        Wallet theWallet = myCustomer.getWallet();
        if (theWallet.getTotalMoney() > payment) {
            theWallet.subtractMoney(payment);
        } else {
            //money not enough
        	throw new RuntimeException("哟呵， 买多了。钱不够");
        }
    }
    
    public void setMyCustomer(Customer myCustomer) {
		this.myCustomer = myCustomer;
	}
    
    public static void main(String[] args) {
    	Paperboy zhangsan = new Paperboy();
    	Wallet wallet = new Wallet();
    	wallet.setTotalMoney(20);
    	Customer customer = new Customer("si", "li", wallet);
    	//买东西
    	float payment = 25;
    	zhangsan.setMyCustomer(customer);
    	System.out.println("开始支付");
    	zhangsan.pay(payment);
    	System.out.println("支付成功");
	}
}
