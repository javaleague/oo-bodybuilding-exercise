package com.javaleague.oo.exercise.moviestore.refactor.movie;

public class NewReleaseMovie extends Movie {

	public NewReleaseMovie(String title) {
		super(title, Movie.NEW_RELEASE);
	}

}
