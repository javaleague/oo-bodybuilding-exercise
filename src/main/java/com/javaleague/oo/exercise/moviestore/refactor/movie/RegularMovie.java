package com.javaleague.oo.exercise.moviestore.refactor.movie;

public class RegularMovie extends Movie {

	public RegularMovie(String title) {
		super(title, Movie.REGULAR);
	}
	
}
