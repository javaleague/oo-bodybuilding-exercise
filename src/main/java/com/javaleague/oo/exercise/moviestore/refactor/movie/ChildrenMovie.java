package com.javaleague.oo.exercise.moviestore.refactor.movie;

public class ChildrenMovie extends Movie {

	public ChildrenMovie(String title) {
		super(title, Movie.CHILDREN);
	}

}
