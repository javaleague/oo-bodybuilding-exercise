package com.javaleague.oo.exercise.moviestore.refactor.rental;

import java.util.List;

public class Rentals {

	private final List<Rental> value;

	public Rentals(List<Rental> value) {
		this.value = value;
	}
	
	public List<Rental> getValue() {
		return value;
	}
	
	public void add(Rental rental) {
		value.add(rental);
	}
	
	public double getTotalAmount() {
		double totalAmount = 0;
		for (int i = 0; i < value.size(); i++) {
			totalAmount += value.get(i).getAmount();
		}
		return totalAmount;
	}

	public int getFrequentRenterPoints() {
		int points = 0;
		for (int i = 0; i < value.size(); i++) {
			points += value.get(i).getFrequentRenterPoints();
		}
		return points;
	}
}
