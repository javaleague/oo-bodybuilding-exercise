package com.javaleague.oo.exercise.moviestore.refactor;


public interface PrintStrategy {
	
	String print(Customer customer);
	
}
