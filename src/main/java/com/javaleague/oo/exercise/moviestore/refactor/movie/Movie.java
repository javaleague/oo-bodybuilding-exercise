package com.javaleague.oo.exercise.moviestore.refactor.movie;

public class Movie {
    public static final int REGULAR = PriceCode.REGULAR.getValue();
    public static final int NEW_RELEASE = PriceCode.NEW_RELEASE.getValue();
    public static final int CHILDREN = PriceCode.CHILDREN.getValue();
    
    private String title;
    private int priceCode;

    public Movie(String title, int priceCode) {
        this.title = title;
        this.priceCode = priceCode;
    }

    public int getPriceCode() {
        return priceCode;
    }

    public String getTitle() {
        return title;
    }
}
