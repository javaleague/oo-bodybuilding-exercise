package com.javaleague.oo.exercise.moviestore.refactor;

import java.util.ArrayList;

import com.javaleague.oo.exercise.moviestore.refactor.movie.ChildrenMovie;
import com.javaleague.oo.exercise.moviestore.refactor.rental.Rental;
import com.javaleague.oo.exercise.moviestore.refactor.rental.Rentals;

class Customer {
    private String name;
    private Rentals rentals = new Rentals(new ArrayList<Rental>());

    public Customer(String name) {
        this.name = name;
    }

    public void addRental(Rental arg) {
        rentals.add(arg);
    }

    public Rentals getRentals() {
		return rentals;
	}
    
    public String getName() {
        return name;
    }

    public String statement() {
        return new DefaultPrintStragety().print(this);
    }
    
    public static void main(String[] args) {
		Customer zhangsan = new Customer("zhangsan");
		zhangsan.addRental(new Rental(new ChildrenMovie("汪汪队第一部"), 1));
		System.out.println(zhangsan.statement());
	}
}
