package com.javaleague.oo.r4.refactor;

import java.util.List;

public class GeneralManager {

	private List<Manager> managers;

	public void doWork() {
		managers.get(0).doWork();
	}

}
