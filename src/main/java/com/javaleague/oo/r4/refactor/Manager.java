package com.javaleague.oo.r4.refactor;

import java.util.List;

public class Manager {
	
	private List<Employee> employees;

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}
	
	public void doWork() {
		employees.get(0).doWork();
	}

}
