package com.javaleague.oo.r3.refactor;

import java.util.Calendar;

public class Example3 {
	
	public static class IntegerWrapper {
		private final Integer value;

		IntegerWrapper(Integer value) {
			this.value = value;
		}

		public Integer getValue() {
			return this.value;
		}
	}
	
	public static class Year extends IntegerWrapper{
		public Year(int value) {
			super(value);
		}
	}
	
	public static class Month extends IntegerWrapper{
		public Month(int value) {
			super(value);
		}
	}
	
	public static class Date extends IntegerWrapper{
		public Date(int value) {
			super(value);
		}
	}
	
	public java.util.Date createDate(Year year, Month month, Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, year.getValue());
		calendar.set(Calendar.MONTH, month.getValue());
		calendar.set(Calendar.DATE, date.getValue());
		return calendar.getTime();
	}
	
}
