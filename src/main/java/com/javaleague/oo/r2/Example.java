package com.javaleague.oo.r2;

public class Example {

	private int status;

	private static final int DONE = 9;

	public void endMe() {
		if (status == DONE) {
			doSomething();
		} else {
			doElse();
		}
	}

	private void doElse() {

	}

	private void doSomething() {

	}
}
