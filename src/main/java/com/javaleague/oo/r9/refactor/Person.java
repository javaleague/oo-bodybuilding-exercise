package com.javaleague.oo.r9.refactor;

public class Person {
	
	private Address address;

	public void setAddress(Address address) {
		this.address = address;
	}
	
	public boolean isLive(String countryName) {
		return address.isInThisContry(countryName);
	}
}
