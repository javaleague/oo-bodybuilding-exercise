package com.javaleague.oo.r9.refactor;

public class Address {

	private String country;

	public void setCountry(String country) {
		this.country = country;
	}

	public boolean isInThisContry(String country) {
		if (this.country.equalsIgnoreCase(country)) {
			return true;
		}
		return false;
	}
}
